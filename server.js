const express = require('express')
const hbs = require('express-handlebars')
const bodyParser = require('body-parser')
const fetch = require('node-fetch')
const path = require('path')
const app = express()

app.engine('hbs', hbs({
  extname: 'hbs',
  defaultLayout: 'layout',
  layoutsDir: path.join(__dirname, '/views/layouts'),
  partialsDir: path.join(__dirname, '/views/partials')
}))

app.set('view engine', 'hbs')

app.use('/css', express.static(path.join(__dirname, '/public/css')))

const jsonParse = bodyParser.json()

app.get('/', (req, res) => {
  fetch('http://localhost:3004/messages')
    .then(response => {
      response.json().then(json => {
        res.render('home', {
          articles: json
        })
        console.log(json)
      })
    })
    .catch(error => {
      console.log(error)
    })
})

app.get('/add_note', (req, res) => {
  res.render('add_note')
})

app.post('/api/add_note', jsonParse, (req, res) => {
  fetch('http://localhost:3004/messages', {
    method: 'POST',
    body: JSON.stringify(req.body),
    headers: {
      'Content-Type': 'application/json'
    }
  }).then((response) => {
    res.status(200).send()
  })
})

app.delete('/api/delete/:id', (req, res) => {
  const id = req.params.id
  fetch(`http://localhost:3004/messages/${id}`, {
    method: 'DELETE'
  }).then((response) => {
    res.status(200).send()
  })
})

app.get('/edit_note/:id', (req, res) => {
  fetch(`http://localhost:3004/messages/${req.params.id}`)
    .then(response => {
      response.json().then(json => {
        res.render('edit_note', {
          article: json
        })
      })
    })
})

app.patch('/api/edit_note/:id', jsonParse, (req, res) => {
  const id = req.params.id

  fetch(`http://localhost:3004/messages/${id}`, {
    method: 'PATCH',
    body: JSON.stringify(req.body),
    headers: {
      'Content-type': 'application/json'
    }
  }).then(response => {
    res.status(200).send()
  })
})

const port = process.env.PORT || 3000

app.listen(port, () => {
  console.log(`Server runing in ${port}`)
})
